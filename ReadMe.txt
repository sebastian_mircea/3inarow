3inaRow

There is only one scene in this game: /Assets/Scenes/Main.unity
All the game logic is in /Assets/Scripts/GameManager.cs.
The players movement is kept using two bit boards and bitwise operations. 
The implementation is found in /Assets/Scripts/BitBoard.cs.
Have fun!
