﻿using UnityEngine;
using UnityEngine.EventSystems;

public class BoardSocket : MonoBehaviour, IDropHandler
{
    #region Serialized Fields
    //
    // The socket's X position on the board
    //
    [SerializeField]
    private sbyte _boardLocationX;

    //
    // The socket's Y position on the board
    //
    [SerializeField]
    private sbyte _boardLocationY;
    #endregion

    #region IDropHandler implementation
    //
    // Handle the OnDrop event
    //
    public void OnDrop(PointerEventData eventData)
    {
        // Get the drop target's rect
        RectTransform currentPosRect = transform as RectTransform;
        // Check to see if the drop occured within the target's rect
        if (RectTransformUtility.RectangleContainsScreenPoint(currentPosRect, Input.mousePosition) && eventData.pointerDrag != null &&
            GameManager.Instance.IsSlotEmpty(_boardLocationX, _boardLocationY))
        {
            // Get the drag handler
            var _dragHandler = eventData.pointerDrag.GetComponent<Marble>();
            if (_dragHandler != null)
            {
                // Check if this is a valid move
                if (!GameManager.Instance.IsMoveValid(_dragHandler, _boardLocationX, _boardLocationY))
                {
                    return;
                }

                // Signal that we have a successful drop
                _dragHandler.SuccessfulDrop = true;
                // Set the drop target as the new parent of the dropped object
                _dragHandler.transform.SetParent(transform);
                // Center the dropped object within the parent
                _dragHandler.transform.localPosition = Vector3.zero;
                // Mark the new position occupied in the game manager
                GameManager.Instance.Move(_dragHandler, _boardLocationX, _boardLocationY);

                // Set the dropped object's new board location
                _dragHandler.BoardLocationX = _boardLocationX;
                _dragHandler.BoardLocationY = _boardLocationY;
            }
        }
    }
    #endregion IDropHandler implementation
}
