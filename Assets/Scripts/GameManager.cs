﻿using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    #region Serialized Fields
    //
    // The panel that is shown when one of the players win
    //
    [SerializeField]
    private VictoryPanel _victoryPanel;

    //
    // The blue marbles
    //
    [SerializeField]
    private Marble[] _blueMarbles;

    //
    // The red marbles
    //
    [SerializeField]
    private Marble[] _redMarbles;
    #endregion Serialized Fields

    #region Private Data
    //
    // Keep track of the position on board using 2 bit boards
    //
    BitBoard _player1;
    BitBoard _player2;

    //
    // Keeps a reference to the curent player
    //
    BitBoard _currentPlayer;
    
    //
    // Counter for the number of moves
    //
    int _moves;
    #endregion Private Data

    #region Public Data
    //
    // Returns the GameManager instance
    //
    private static GameManager _instance = null;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    #endregion Public Data

    #region Public Methods
    //
    // Start a new game
    //
    public void StartNewGame()
    {
        // Reset the moves counter
        _moves = 0;
        // Reset the bord positions
        _player1.Reset();
        _player2.Reset();
        // Reset all marbles to their original position
        foreach (var marble in _blueMarbles)
        {
            marble.ResetPositionAndState();
        }
        foreach (var marble in _redMarbles)
        {
            marble.ResetPositionAndState();
        }
        // Hide the victory panel
        _victoryPanel.HideVictoryScreen();
    }

    //
    // Check if the designated slot is empty
    //
    public bool IsSlotEmpty(int x, int y)
    {
        return (!_player1.Check(x, y) && !_player2.Check(x, y));
    }

    //
    // Record a move to the specified position and clear the previous one if needed
    //
    public void Move(Marble movedMarble, int toX, int toY)
    {
        _moves++;
        if (_moves % 2 != 0)
        {
            _currentPlayer = _player1;
        }
        else
        {
            _currentPlayer = _player2;
        }
            
        // Check if the piece is already on the board
        if (movedMarble.BoardLocationX >=0 && movedMarble.BoardLocationY >=0)
        {
            // Clear the previous position
            _currentPlayer.Clear(movedMarble.BoardLocationX, movedMarble.BoardLocationY);
        }
        // Mark the move to the next position
        _currentPlayer.Move(toX, toY);


        // Check for win condition
        if (_moves >= 5)
        {
            if (_currentPlayer.WinConditionMet())
            {
                movedMarble.InvertDraggableState(false);

                // Show victory screen
                _victoryPanel.ShowVictoryScreen(_currentPlayer.PlayerName);
                return;
            }
        }

        if (_moves > 1 && _moves < 6 || _moves > 6)
        {
            // Invert the drag state for all the marbles
            InvertMarblesDraggableState(_blueMarbles, movedMarble);
            InvertMarblesDraggableState(_redMarbles, movedMarble);
        }
        else if (_moves == 1)
        {
            // This is the first move; we will make unselectable the marbles the player has choosen
            Marble[] marbles;
            if (movedMarble.gameObject.tag == "BlueMarble")
            {
                marbles = _blueMarbles;
            }
            else
            {
                marbles = _redMarbles;
            }
            InvertMarblesDraggableState(marbles, movedMarble);
        }
        else if (_moves == 6)
        {
            // Whit this move completed, all the marbles will be on the board; make the oposite player's marbles movable
            Marble[] marbles;
            if (movedMarble.gameObject.tag == "BlueMarble")
            {
                marbles = _redMarbles;
            }
            else
            {
                marbles = _blueMarbles;
            }
            InvertMarblesDraggableState(marbles, movedMarble);

            movedMarble.InvertDraggableState(false);
        }
    }

    //
    // Check if the move to the specified position in valid
    //
    public bool IsMoveValid(Marble movedMarble, int toX, int toY)
    {
        // Check if all the marbles are on the board
        if (_moves >= 6)
        {
            int x = movedMarble.BoardLocationX;
            int y = movedMarble.BoardLocationY;
            if ((Math.Abs(x - toX) == 1 || Math.Abs(y - toY) == 1) // movement is no longer than 1 unit
                && ((x == toX || y == toY) || (x == y && toX == toY) || (x == 2 - y && toX == 2 - toY))) // and moving on the same row, the same column or on the main diagonals
            {
                return true;
            }
            return false;
        }
        return true;
    }
    #endregion Public Methods

    #region Private Methods
    //
    // Invert draggable state
    //
    private void InvertMarblesDraggableState(Marble[] marbles, Marble movedMarble)
    {
        foreach (var marble in marbles)
        {
            if (marble != movedMarble)
            {
                // If not all the marbles are on board
                if (_moves < 6)
                {
                    // Invert draggable state only for marbles on the board
                    if (marble.transform.parent.gameObject.tag != "BoardPocket")
                    {
                        marble.InvertDraggableState();
                    }
                }
                else
                {
                    marble.InvertDraggableState();
                }
            }
            else
            {
                // We do not need to make the currently moved marble unselectalbe here, we only gray it out
                marble.InvertDraggableState(false);
            }
        }
    }
    #endregion Private Methods

    #region MonoBehaviour Life Cycle
    //
    // MonoBehaviour's Awake
    //
    private void Awake()
    {
        if (_victoryPanel == null)
        {
            Debug.LogError("A reference to the Victory Panel not set in the GameManager!");
            return;
        }

        // Make sure we got an instance awailable of the GameManager singleton
        if (_instance == null)
        {
            _instance = this;
        }
        else if (_instance != this)
        {
            // destroying this enforces the singleton pattern
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    //
    // MonoBehaviour's Start
    //
    void Start ()
    {
        // Initialize the bit boards for the two players
        _player1 = new BitBoard("Player 1");
        _player2 = new BitBoard("Player 2");

        // Start a new game
        StartNewGame();
	}
    #endregion MonoBehaviour Life Cycle
}
