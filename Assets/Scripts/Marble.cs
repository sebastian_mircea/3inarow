﻿using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class Marble : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    #region Public Data
    //
    // Keeps track of the drop status
    //
    public bool SuccessfulDrop
    {
        get; set;
    }

    //
    // The current X location of the object on the board
    //
    public sbyte BoardLocationX
    {
        get; set;
    }

    //
    // The current Y location of the object on the board
    //
    public sbyte BoardLocationY
    {
        get; set;
    }
    #endregion Public Data

    #region Private Data
    //
    // The position from which the drag action started
    //
    private Vector3 _dragStartPosition;

    //
    //The current canvas group -  used to set/reset the "blocksRaycasts" during drag&drop
    //
    private CanvasGroup _canvasGroup;

    //
    // Initial offboard location - used when starting a new game
    //
    private Transform _offBoardParent;
    #endregion Private Data

    #region Public Methods
    //
    // Inverts the draggable state
    //
    public void InvertDraggableState(bool invertBlockRaycasts = true)
    {
        if (invertBlockRaycasts)
        {
            _canvasGroup.blocksRaycasts = !_canvasGroup.blocksRaycasts;
        }
        _canvasGroup.alpha = _canvasGroup.alpha == 1 ? 0.5f : 1f;
    }

    //
    // Reset the marble to it's original position and activate the draggable state
    //
    public void ResetPositionAndState()
    {
        transform.SetParent(_offBoardParent);
        transform.localPosition = Vector3.zero;
        _canvasGroup.blocksRaycasts = true;
        _canvasGroup.alpha = 1;
    }    
    #endregion Public Methods

    #region IDragHandler implementation
    //
    // Handle dragging
    //
    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        // move the dragged transform to the current mouse position
        transform.position = Input.mousePosition;
    }
    #endregion IDragHandler implementation

    #region IBeginDragHandler implementation
    //
    // BeginDrag event handler
    //
    public void OnBeginDrag(PointerEventData eventData)
    {
        // Check if the current player is allowed to drag this object

        // set the "drop succeded" flag to false
        SuccessfulDrop = false;
        // store the initial position of the dragged object
        _dragStartPosition = transform.localPosition;
        // allow raycasts to pass through(this is needed to allow the drop handler to be triggerd)
        _canvasGroup.blocksRaycasts = false;
    }
    #endregion IBeginDragHandler implementation

    #region IEndDragHandler implementation
    //
    // EndDrag event handler
    //
    public void OnEndDrag(PointerEventData eventData)
    {
        if (!SuccessfulDrop)
        {
            // Reset the raycasts block flag
            _canvasGroup.blocksRaycasts = true;

            // In case of an unsuccessful drop, reset the object to its initial position
            transform.localPosition = _dragStartPosition;
        }
    }
    #endregion IEndDragHandler implementation

    #region MonoBehaviour life cycle
    private void Awake()
    {
        // Retreive the canvas group component(needed for switching the "blocksRaycasts" flag on and off)
        _canvasGroup = GetComponent<CanvasGroup>();
        // Store the initial offboard location(used when starting a new game)
        _offBoardParent = transform.parent;
    }

    private void Start()
    {
        // Init the location to (-1,-1) as the object is not yet placed on the board
        BoardLocationX = -1;
        BoardLocationY = -1;
    }
    #endregion MonoBehaviour life cycle
}
