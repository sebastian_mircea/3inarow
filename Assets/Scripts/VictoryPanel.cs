﻿using UnityEngine;
using UnityEngine.UI;

public class VictoryPanel : MonoBehaviour
{
    //
    // The name of the winner
    //
    [SerializeField]
    private Text _playerName;

    //
    // Shows the victory screen
    //
    public void ShowVictoryScreen(string winnerName)
    {
        // Set the player's name on the screen
        _playerName.text = winnerName + " is the winner!";

        // Activate the victory panel
        transform.parent.gameObject.SetActive(true);
        gameObject.SetActive(true);
    }

    //
    // Hide the victory screen
    //
    public void HideVictoryScreen()
    {
        // Activate the victory panel
        transform.parent.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
