﻿using System;

public class BitBoard
{
    #region Private Data
    // Keep the player's position on the bord using an int and setting its bits in binary
    // Example: 
    // For pos(0,0) the board looks like this: 
    // 1 0 0
    // 0 0 0
    // 0 0 0
    // so we will set the first bit of our int to 1:        _board = 000000001,
    // for pos(1,0)  we will set the second bit to 1:       _board = 000000010,
    // for pos(0,1) we will set the forth bit to 1:         _board = 000001000
    // and so on
    private int _board = 0;

    //
    // The name of the player owning this board
    //
    private string _playerName;
    #endregion Private Data

    #region Public Data
    //
    // Getter for the Player's name
    //
    public string  PlayerName
    {
        get { return _playerName; }
    }
    #endregion Public Data

    #region Public Methods
    //
    // Constructor
    //
    public BitBoard(string playerName)
    {
        _playerName = playerName;
    }

    //
    // Make a move in this (x, y) location
    //
    public void Move(int x, int y)
    {
        // We will use "|" operator and the binary left shift operator to set the bit coresponding to (x,y) to the value of 1
        _board |= (1 << y * 3 + x);
    }

    //
    // Clear the bit for the indicated location
    //
    public void Clear(int x, int y)
    {
        _board &= ~(1 << y * 3 + x);
    }

    //
    // Check if there is any of our piece placed in this (x, y) location
    //
    public bool Check(int x, int y)
    {
        return (_board & (1 << y * 3 + x)) > 0;
    }

    //
    // Reset the content of the board
    //
    public void Reset()
    {
        _board = 0;
    }

    //
    // Check if we have a winning condition
    //
    public bool WinConditionMet()
    {
        return (_board & 292) == 292    // 100100100 - first column
            || (_board & 146) == 146    // ‭010010010‬ - second column
            || (_board & 73) == 73      // ‭001001001‬ - third column
            || (_board & 448) == 448    // ‭111000000‬ - first row
            || (_board & 56) == 56      // ‭000111000‬ - second row
            || (_board & 7) == 7        // ‭000000111‬ - third row
            || (_board & 273) == 273    // 100010001‬ - first diagonal
            || (_board & 84) == 84;     // ‭001010100‬ - second diagonal
    }
    #endregion Public Methods
};